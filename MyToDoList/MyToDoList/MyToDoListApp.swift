//
//  MyToDoListApp.swift
//  MyToDoList
//
//  Created by Andrei Albu on 13.04.2024.
//

import SwiftUI

@main
struct MyToDoListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
